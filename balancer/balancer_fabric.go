package balancer

import (
	"bitbucket.org/lazadaweb/discovery/discovery"
	"bitbucket.org/lazadaweb/discovery/discovery/etcd"

	etcdcl "github.com/coreos/etcd/client"
)

// IBalancerFabric implements GetBalancerForService method
type IBalancerFabric interface {
	GetBalancerForService(name string) LoadBalancer
}

type balancerFabric struct {
	namespace   string
	venture     string
	environment string
	logger      discovery.ILogger
	client      etcdcl.Client
}

var _ IBalancerFabric = (*balancerFabric)(nil)

// NewRoundRobinBalancerFabric creates balancer fabric of round robin balancers
func NewRoundRobinBalancerFabric(logger discovery.ILogger, client etcdcl.Client,
	namespace, venture, environment string) IBalancerFabric {

	return &balancerFabric{
		namespace:   namespace,
		venture:     venture,
		environment: environment,
		logger:      logger,
		client:      client,
	}
}

// GetBalancerForService returns LoadBalancer for service by name
func (this *balancerFabric) GetBalancerForService(name string) LoadBalancer {
	return NewRoundRobin(this.logger, etcd.NewLocator(this.client, this.logger), discovery.LocationInfo{
		Namespace:   this.namespace,
		Venture:     this.venture,
		Environment: this.environment,
		ServiceName: name,
	})
}

// IBalancerRegestry implements GetServiceAddrByName
type IBalancerRegestry interface {
	GetServiceAddrByName(name string) (string, error)
}

type balancerRegestry struct {
	namespace   string
	venture     string
	environment string
	logger      discovery.ILogger
	client      etcdcl.Client
	balancers   map[string]LoadBalancer
}

var _ IBalancerRegestry = (*balancerRegestry)(nil)

// NewRoundRobinBalancerRegestry creates round robin balancer
func NewRoundRobinBalancerRegestry(logger discovery.ILogger, client etcdcl.Client,
	namespace, venture, environment string) IBalancerRegestry {

	return &balancerRegestry{
		namespace:   namespace,
		venture:     venture,
		environment: environment,
		logger:      logger,
		client:      client,
		balancers:   make(map[string]LoadBalancer, 1),
	}
}

// GetServiceAddrByName returns service addr by name
func (this *balancerRegestry) GetServiceAddrByName(name string) (string, error) {
	var balancer LoadBalancer
	if item, ok := this.balancers[name]; ok {
		balancer = item
	} else {
		balancer = this.create(name)
		this.balancers[name] = balancer
	}
	return balancer.Next()
}

func (this *balancerRegestry) create(name string) LoadBalancer {
	return NewRoundRobin(this.logger, etcd.NewLocator(this.client, this.logger),
		discovery.LocationInfo{
			Namespace:   this.namespace,
			Venture:     this.venture,
			Environment: this.environment,
			ServiceName: name,
		})
}
