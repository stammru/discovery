package balancer

import (
	"fmt"
	"net"
	"net/http"
	"time"
)

// TODO: https://github.com/golang/go/issues/9405

// HealthCheck sends GET request "/health_check" and waits for 200 response and returns round trip time.
// If server responses with bad code (503, whatever), request timeouts etc. health check fails.
func HealthCheck(url string, timeout time.Duration) error {
	client := &http.Client{
		Transport: &http.Transport{
			Dial: (&net.Dialer{
				Timeout: timeout,
			}).Dial,
			TLSHandshakeTimeout: 10 * time.Second,
			DisableKeepAlives:   true,
		},
		Timeout: timeout,
	}

	// TODO: hardcoded url
	resp, err := client.Get(url + "/health_check")
	if err != nil {
		return err
	}

	if err := resp.Body.Close(); err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("bad status code: %d", resp.StatusCode)
	}

	return nil
}
