package balancer

import (
	"sync"
	"time"

	"bitbucket.org/lazadaweb/discovery/discovery"
	etcd "github.com/coreos/etcd/client"
	"golang.org/x/net/context"
)

type RoundRobin struct {
	serviceName string
	nodes       Nodes
	// Index of current rolling service used for round-robin balancing
	index     int
	ready     chan struct{}
	done      chan struct{}
	readyOnce sync.Once
	stopOnce  sync.Once
	mx        sync.RWMutex

	logger discovery.ILogger
}

var _ LoadBalancer = (*RoundRobin)(nil)

func NewRoundRobin(logger discovery.ILogger, locator discovery.IServiceLocator2, info discovery.LocationInfo) *RoundRobin {
	info.Property = discovery.NodesProperty

	b := &RoundRobin{
		logger:      logger,
		serviceName: info.ServiceName,
		index:       -1,
		ready:       make(chan struct{}),
		done:        make(chan struct{}),
	}

	go b.consume(locator, info)

	return b
}

func (b *RoundRobin) Next() (string, error) {
	if err := b.waitForInitializing(); err != nil {
		return "", err
	}

	b.mx.Lock()
	defer b.mx.Unlock()

	i, err := b.nextIndex()
	if err != nil {
		return "", err
	}
	return b.nodes[i].URL, nil
}

func (b *RoundRobin) NextN(n int) ([]string, error) {
	if err := b.waitForInitializing(); err != nil {
		return nil, err
	}

	b.mx.Lock()
	defer b.mx.Unlock()

	i, err := b.nextIndex()
	if err != nil {
		return nil, err
	}
	count := len(b.nodes)
	if n > count {
		n = count
	}
	urls := make([]string, n)
	for k := 0; k < n; k++ {
		urls[k] = b.nodes[i].URL
		i = (i + 1) % count
	}
	return urls, nil
}

func (b *RoundRobin) nextIndex() (int, error) {
	count := len(b.nodes)
	if count == 0 {
		return 0, discovery.ErrNoServiceAvailable
	}
	i := b.index
	i = (i + 1) % count
	b.index = i
	b.nodes[i].count++
	return i, nil
}

func (b *RoundRobin) waitForInitializing() (err error) {
	select {
	case <-b.done:
		b.logger.Debugf("%q is stopped.", b.serviceName)
		err = discovery.ErrNoServiceAvailable
	default:
	}
	select {
	case <-b.ready:
	// Initialized. Ready to work.
	case <-time.After(initTimeout):
		b.logger.Errorf("failed to receive updates for %q during %s timeout.", b.serviceName, initTimeout)
		err = discovery.ErrNoServiceAvailable
	}
	return
}

func (b *RoundRobin) Stats() []NodeStat {
	var stats []NodeStat

	b.mx.RLock()
	if len(b.nodes) > 0 {
		p := 100. / float64(len(b.nodes))
		for _, v := range b.nodes {
			stats = append(stats, NodeStat{
				Key:            v.Key,
				Value:          v.URL,
				Healthy:        true,
				HitCount:       v.count,
				HitProbability: p,
				RTT:            0,
				RTTAverage:     0,
			})
		}
	}
	b.mx.RUnlock()

	return stats
}

func (b *RoundRobin) Stop() {
	b.stopOnce.Do(b.safeStop)
}

func (b *RoundRobin) safeStop() {
	close(b.done)
}

func (b *RoundRobin) consume(locator discovery.IServiceLocator2, info discovery.LocationInfo) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	in := make(chan *etcd.Response, 1)
	go locator.Locate(ctx, info, in)

	for {
		select {
		case resp, ok := <-in:
			if !ok {
				b.logger.Error("locator channel closed unexpectedly")
				return
			}
			b.handleResponse(resp)
		case <-b.done:
			return
		}
	}
}

func (b *RoundRobin) handleResponse(resp *etcd.Response) {
	switch resp.Action {
	case "get":
		b.replaceNodes(resp)

		b.readyOnce.Do(func() {
			close(b.ready)
		})
	case "create":
		b.addNode(resp)
	case "update":
		b.updateNode(resp, false)
	case "set":
		b.updateNode(resp, true)
	case "delete", "expire":
		b.deleteNode(resp)
	}
}

func (b *RoundRobin) replaceNodes(resp *etcd.Response) {
	nodes := make(Nodes, len(resp.Node.Nodes))
	for i, etcdNode := range resp.Node.Nodes {
		nodes[i] = newNode(etcdNode.Key, etcdNode.Value)
	}
	b.mx.Lock()
	b.nodes = nodes
	b.mx.Unlock()
}

func (b *RoundRobin) addNode(resp *etcd.Response) {
	node := newNode(resp.Node.Key, resp.Node.Value)
	b.mx.Lock()
	b.nodes = append(b.nodes, node)
	b.mx.Unlock()
}

func (b *RoundRobin) updateNode(resp *etcd.Response, create bool) {
	b.mx.Lock()
	defer b.mx.Unlock()

	i := b.nodes.indexOf(resp.Node.Key)
	if i == -1 {
		if create {
			node := newNode(resp.Node.Key, resp.Node.Value)
			b.nodes = append(b.nodes, node)
		}
		return
	}

	oldNode := b.nodes[i]
	if oldNode.URL != resp.Node.Value {
		b.nodes[i] = newNode(resp.Node.Key, resp.Node.Value)
	}
}

func (b *RoundRobin) deleteNode(resp *etcd.Response) {
	b.mx.Lock()
	i := b.nodes.indexOf(resp.Node.Key)
	if i != -1 {
		b.nodes = b.nodes.removeByIndex(i)
	}
	b.mx.Unlock()
}
