package balancer

import (
	"bitbucket.org/lazadaweb/discovery/discovery"
	etcd "github.com/coreos/etcd/client"
	"golang.org/x/net/context"
)

var _ Producer = (*EtcdProducer)(nil)

// EtcdProducer is DEPRECATED.
type EtcdProducer struct {
	source chan *etcd.Response
	cancel context.CancelFunc
}

func NewEtcdProducer2(locator discovery.IServiceLocator2, info discovery.LocationInfo) *EtcdProducer {
	info.Property = discovery.NodesProperty
	ctx, cancel := context.WithCancel(context.TODO())
	out := make(chan *etcd.Response)
	go locator.Locate(ctx, info, out)
	return &EtcdProducer{
		source: out,
		cancel: cancel,
	}
}

func NewEtcdProducer(client discovery.IClient, info discovery.LocationInfo, logger discovery.ILogger) *EtcdProducer {
	info.Output = make(chan *etcd.Response)
	info.Property = discovery.NodesProperty

	locator := client.NewLocator(info, logger)
	return &EtcdProducer{
		source: info.Output,
		cancel: func() { locator.Close() },
	}
}

func (e *EtcdProducer) Stream() chan *etcd.Response {
	return e.source
}

func (e *EtcdProducer) Stop() {
	e.cancel()
}
