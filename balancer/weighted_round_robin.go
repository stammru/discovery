package balancer

import (
	"sort"
	"sync"
	"time"

	"bitbucket.org/lazadaweb/discovery/discovery"
	etcd "github.com/coreos/etcd/client"
	"golang.org/x/net/context"
)

const (
	initTimeout         = time.Millisecond * 500
	healthcheckInterval = time.Millisecond * 1000
	healthcheckTimeout  = time.Millisecond * 500
	maxResponseTime     = time.Minute
)

type CheckerFunc func(url string, timeout time.Duration) error

type WeightedRoundRobin struct {
	serviceName string

	checker CheckerFunc

	nodes Nodes

	// Index of current rolling service used for round-robin balancing
	index int
	// current weight
	cw        uint64
	gcdVal    uint64
	maxWeight uint64
	sumWeight uint64

	ready     chan struct{}
	done      chan struct{}
	readyOnce sync.Once
	stopOnce  sync.Once
	mx        sync.RWMutex

	logger discovery.ILogger
}

var _ LoadBalancer = (*WeightedRoundRobin)(nil)

func NewWeightedRoundRobin(logger discovery.ILogger, locator discovery.IServiceLocator2, info discovery.LocationInfo) *WeightedRoundRobin {
	info.Property = discovery.NodesProperty

	b := &WeightedRoundRobin{
		logger:      logger,
		serviceName: info.ServiceName,
		checker:     DialCheck,
		index:       -1,
		cw:          0,
		ready:       make(chan struct{}),
		done:        make(chan struct{}),
	}

	go b.consume(locator, info)
	return b
}

func (b *WeightedRoundRobin) consume(locator discovery.IServiceLocator2, info discovery.LocationInfo) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	in := make(chan *etcd.Response, 1)
	go locator.Locate(ctx, info, in)

	for {
		select {
		case resp, ok := <-in:
			if !ok {
				b.logger.Error("locator channel closed unexpectedly")
				return
			}
			b.handleResponse(resp)
		case <-b.done:
			return
		}
	}
}

func (b *WeightedRoundRobin) handleResponse(resp *etcd.Response) {
	switch resp.Action {
	case "get":
		b.replaceNodes(resp)

		b.readyOnce.Do(func() {
			go b.healthchecker()
			close(b.ready)
		})
	case "create":
		b.addNode(resp)
	case "update":
		b.updateNode(resp, false)
	case "set":
		b.updateNode(resp, true)
	case "delete", "expire":
		b.deleteNode(resp)
	}
}

func (b *WeightedRoundRobin) replaceNodes(resp *etcd.Response) {
	nodes := make(Nodes, len(resp.Node.Nodes))
	for i, etcdNode := range resp.Node.Nodes {
		nodes[i] = newNode(etcdNode.Key, etcdNode.Value)
	}
	b.mx.Lock()
	defer b.mx.Unlock()

	b.nodes = nodes
	b.updateWeights()
}

func (b *WeightedRoundRobin) addNode(resp *etcd.Response) {
	node := newNode(resp.Node.Key, resp.Node.Value)
	b.mx.Lock()
	defer b.mx.Unlock()

	// add new node to the end because it has max response time by default
	b.nodes = append(b.nodes, node)
	b.updateWeights()
}

func (b *WeightedRoundRobin) updateNode(resp *etcd.Response, create bool) {
	b.mx.Lock()
	defer b.mx.Unlock()

	i := b.nodes.indexOf(resp.Node.Key)
	if i == -1 {
		if create {
			node := newNode(resp.Node.Key, resp.Node.Value)
			// add new node to the end because it has max response time by default
			b.nodes = append(b.nodes, node)
			b.updateWeights()
		}
		return
	}

	oldNode := b.nodes[i]
	if oldNode.URL != resp.Node.Value {
		node := newNode(resp.Node.Key, resp.Node.Value)
		// shift left and add new node to the end because it has max response time by default
		b.nodes = b.nodes.removeByIndex(i)
		b.nodes = append(b.nodes, node)
		b.updateWeights()
	}
}

func (b *WeightedRoundRobin) deleteNode(resp *etcd.Response) {
	b.mx.Lock()
	defer b.mx.Unlock()

	i := b.nodes.indexOf(resp.Node.Key)
	if i != -1 {
		b.nodes = b.nodes.removeByIndex(i)
		b.updateWeights()
	}
}

type healthcheckStat struct {
	RTT   time.Duration
	State nodeState
}

func (b *WeightedRoundRobin) healthchecker() {
	for {
		var copiedNodes Nodes
		var stats []healthcheckStat
		var wg sync.WaitGroup

		b.mx.RLock()
		count := len(b.nodes)
		if count == 0 {
			b.mx.RUnlock()
			goto SLEEP
		}
		copiedNodes = make(Nodes, count)
		copy(copiedNodes, b.nodes)
		b.mx.RUnlock()

		stats = make([]healthcheckStat, count)
		wg.Add(count)
		for i, node := range copiedNodes {
			go func(url string, stat *healthcheckStat) {
				defer wg.Done()
				b.checkNode(url, stat)
			}(node.URL, &stats[i])
		}

		go func() {
			wg.Wait()

			select {
			case <-b.done:
				return
			default:
			}

			b.mx.Lock()
			defer b.mx.Unlock()

			for i, node := range copiedNodes {
				r := &stats[i]
				node.state = r.State
				node.stats.Add(r.RTT)
			}
			sort.Sort(b.nodes)
			b.updateWeights()
		}()

	SLEEP:
		select {
		case <-b.done:
			return
		case <-time.After(healthcheckInterval):
		}
	}
}

func (b *WeightedRoundRobin) checkNode(url string, stat *healthcheckStat) {
	start := time.Now()
	if err := b.checker(url, healthcheckTimeout); err != nil {
		stat.RTT = maxResponseTime
		stat.State = Unhealthy
	} else {
		stat.RTT = time.Since(start)
		stat.State = Healthy
	}
}

func (b *WeightedRoundRobin) updateWeights() {
	if len(b.nodes) == 0 {
		b.index = -1
		b.cw = 0
		b.maxWeight = 0
		b.sumWeight = 0
		return
	}

	/*
		// 1.5 is empiracally chosen coefficient
		n := nextPowerOfTwo(3 * len(b.nodes) / 2)
		minRTT := b.nodes[0].stats.avg
		b.sumWeight = 0

		for _, node := range b.nodes {
			// sumRTT / nodeRTT / maxWeight where maxWeight = sumRTT / minRTT * numOfNodes
			node.weight = uint64(float64(minRTT) / float64(node.stats.avg) * float64(n))
			b.sumWeight += node.weight
		}

		b.maxWeight = b.nodes[0].weight
		if b.cw > b.maxWeight {
			b.index = -1
			b.cw = 0
		}
	*/

	var sumRTT uint64
	for _, node := range b.nodes {
		sumRTT += uint64(node.stats.avg)
	}

	var sumWeight uint64
	for _, node := range b.nodes {
		node.weight = uint64((float64(sumRTT) / float64(node.stats.avg)) + 0.5)
		sumWeight += node.weight
	}

	/*
		n := nextPowerOfTwo(len(b.nodes))
		k := float64(sumWeight) / float64(n)
		sumWeight = 0
		for _, node := range b.nodes {
			node.weight = uint64(float64(node.weight)/k + 0.5)
			sumWeight += node.weight
		}
	*/
	b.sumWeight = sumWeight

	b.maxWeight = b.nodes[0].weight
	if b.cw > b.maxWeight {
		b.index = -1
		b.cw = 0
	}
}

func (b *WeightedRoundRobin) Next() (string, error) {
	if err := b.waitForInitializing(); err != nil {
		return "", err
	}

	b.mx.Lock()
	defer b.mx.Unlock()

	i, err := b.nextIndex()
	if err != nil {
		return "", err
	}
	return b.nodes[i].URL, nil
}

func (b *WeightedRoundRobin) NextN(n int) ([]string, error) {
	if err := b.waitForInitializing(); err != nil {
		return nil, err
	}

	b.mx.Lock()
	defer b.mx.Unlock()

	i, err := b.nextIndex()
	if err != nil {
		return nil, err
	}
	count := len(b.nodes)
	if n > count {
		n = count
	}
	urls := make([]string, n)
	for k := 0; k < n; k++ {
		urls[k] = b.nodes[i].URL
		i = (i + 1) % count
	}
	return urls, nil
}

func (b *WeightedRoundRobin) nextIndex() (int, error) {
	count := len(b.nodes)
	if count == 0 || b.maxWeight == 0 {
		return 0, discovery.ErrNoServiceAvailable
	}

	i := b.index
	cw := b.cw

	i = (i + 1) % len(b.nodes)
	if i == 0 {
		if cw <= 1 {
			cw = b.maxWeight
		} else {
			cw--
		}
	}

	node := b.nodes[i]
	if node.weight < cw {
		// all weights are less than cw and we need to start over
		i = 0
		node = b.nodes[0]
		if cw <= 1 {
			cw = b.maxWeight
		} else {
			cw--
		}
	}

	b.index = i
	b.cw = cw
	node.count++
	return i, nil
}

func (b *WeightedRoundRobin) waitForInitializing() (err error) {
	select {
	case <-b.done:
		b.logger.Debugf("%q is stopped.", b.serviceName)
		err = discovery.ErrNoServiceAvailable
	default:
	}
	select {
	case <-b.ready:
		// Initialized. Ready to work.
	case <-time.After(initTimeout):
		b.logger.Errorf("failed to receive updates for %q during %s timeout.", b.serviceName, initTimeout)
		err = discovery.ErrNoServiceAvailable
	}
	return
}

func (b *WeightedRoundRobin) Stats() []NodeStat {
	var stats []NodeStat

	b.mx.RLock()
	for _, v := range b.nodes {
		healthy := v.state == Healthy
		p := float64(v.weight) / float64(b.sumWeight)
		stats = append(stats, NodeStat{
			Key:            v.Key,
			Value:          v.URL,
			Healthy:        healthy,
			HitCount:       v.count,
			HitProbability: p,
			RTT:            v.stats.Current(),
			RTTAverage:     v.stats.avg,
		})
	}
	b.mx.RUnlock()

	return stats
}

func (b *WeightedRoundRobin) safeStop() {
	close(b.done)
}

func (b *WeightedRoundRobin) Stop() {
	b.stopOnce.Do(b.safeStop)
}

//func nextPowerOfTwo(n int) int {
//	n--
//	n |= n >> 1
//	n |= n >> 2
//	n |= n >> 4
//	n |= n >> 8
//	n |= n >> 16
//	n++
//	return n
//}
