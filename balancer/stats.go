package balancer

import "time"

type NodeStat struct {
	Key            string        `json:"key"`
	Value          string        `json:"value"`
	Healthy        bool          `json:"healthy"`
	HitProbability float64       `json:"hitProbability"`
	HitCount       uint64        `json:"hitCount"`
	RTT            time.Duration `json:"rtt"`
	RTTAverage     time.Duration `json:"rttAverage"`
}

type StatsByKey []NodeStat

func (s StatsByKey) Len() int {
	return len(s)
}

func (s StatsByKey) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (s StatsByKey) Less(i, j int) bool {
	return s[i].Key < s[j].Key
}

type StatsByValue []NodeStat

func (s StatsByValue) Len() int {
	return len(s)
}

func (s StatsByValue) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (s StatsByValue) Less(i, j int) bool {
	return s[i].Value < s[j].Value
}

type StatsByProbability []NodeStat

func (s StatsByProbability) Len() int {
	return len(s)
}

func (s StatsByProbability) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (s StatsByProbability) Less(i, j int) bool {
	return s[i].HitProbability < s[j].HitProbability
}
