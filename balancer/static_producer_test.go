package balancer

import (
	"testing"

	"bitbucket.org/lazadaweb/discovery/discovery"
	etcd "github.com/coreos/etcd/client"
)

func TestStaticProducerFromFile(t *testing.T) {
	info1 := discovery.LocationInfo{
		Namespace:   "discovery_test",
		Venture:     "my",
		Environment: "test",
		ServiceName: "myservice1",
	}
	producer1, err := StaticProducerFromFile("test_static_nodes.ini", info1)
	if err != nil {
		t.Fatal(err)
	}
	assertProducerURLs(t, producer1, []string{"url1", "url2"})

	info2 := discovery.LocationInfo{
		Namespace:   "discovery_test",
		Venture:     "my",
		Environment: "test",
		ServiceName: "myservice2",
	}
	producer2, err := StaticProducerFromFile("test_static_nodes.ini", info2)
	if err != nil {
		t.Fatal(err)
	}
	assertProducerURLs(t, producer2, []string{"url1"})
}

func assertProducerURLs(t *testing.T, producer *StaticProducer, expectedURLs []string) {
	var resp *etcd.Response
	select {
	case resp = <-producer.Stream():
	default:
		t.Fatal("producer stream is empty")
	}

	if resp.Action != "get" {
		t.Fatalf("wrong response action: %s", resp.Action)
	}

	if len(resp.Node.Nodes) != len(expectedURLs) {
		t.Fatalf("response length is %d, expected %d", len(resp.Node.Nodes), len(expectedURLs))
	}
	for i, node := range resp.Node.Nodes {
		if node.Value != expectedURLs[i] {
			t.Fatalf("url %q, expected %q", node.Value, expectedURLs[i])
		}
	}
}
