package balancer

type LoadBalancer interface {
	Next() (string, error)
	NextN(n int) ([]string, error)
	Stats() []NodeStat
	Stop()
}
