package balancer

import (
	"testing"

	"bitbucket.org/lazadaweb/discovery/discovery"
	"bitbucket.org/lazadaweb/go-logger"
)

func TestRoundRobin_Next(t *testing.T) {
	balancer := createTestRRBalancer()
	defer balancer.Stop()

	if url, _ := balancer.Next(); url != "1" {
		t.Fail()
	}
	if url, _ := balancer.Next(); url != "2" {
		t.Fail()
	}
	if url, _ := balancer.Next(); url != "3" {
		t.Fail()
	}
	if url, _ := balancer.Next(); url != "1" {
		t.Fail()
	}
	if url, _ := balancer.Next(); url != "2" {
		t.Fail()
	}
	if url, _ := balancer.Next(); url != "3" {
		t.Fail()
	}
}

func TestRoundRobin_NextN1(t *testing.T) {
	balancer := createTestRRBalancer()
	defer balancer.Stop()

	assertNextN(t, balancer, 1, "1")
	assertNextN(t, balancer, 1, "2")
	assertNextN(t, balancer, 1, "3")
	assertNextN(t, balancer, 1, "1")
}

func TestRoundRobin_NextN2(t *testing.T) {
	balancer := createTestRRBalancer()
	defer balancer.Stop()

	assertNextN(t, balancer, 2, "1", "2")
	assertNextN(t, balancer, 2, "2", "3")
	assertNextN(t, balancer, 2, "3", "1")
	assertNextN(t, balancer, 2, "1", "2")
}

func TestRoundRobin_NextN3(t *testing.T) {
	balancer := createTestRRBalancer()
	defer balancer.Stop()

	assertNextN(t, balancer, 3, "1", "2", "3")
	assertNextN(t, balancer, 3, "2", "3", "1")
	assertNextN(t, balancer, 3, "3", "1", "2")
	assertNextN(t, balancer, 3, "1", "2", "3")
}

func TestRoundRobin_NextN4(t *testing.T) {
	balancer := createTestRRBalancer()
	defer balancer.Stop()

	assertNextN(t, balancer, 4, "1", "2", "3")
	assertNextN(t, balancer, 4, "2", "3", "1")
	assertNextN(t, balancer, 4, "3", "1", "2")
	assertNextN(t, balancer, 4, "1", "2", "3")
}

func assertNextN(t *testing.T, balancer LoadBalancer, n int, expectedURLs ...string) {
	urls, err := balancer.NextN(n)
	if err != nil {
		t.Fatal(err)
	}
	assertURLs(t, urls, expectedURLs)
}

func assertURLs(t *testing.T, urls, expectedURLs []string) {
	if len(urls) != len(expectedURLs) {
		t.Fatalf("actual urls of size %d, expected urls of size %d", len(urls), len(expectedURLs))
	}
	for i, url := range urls {
		expectedURL := expectedURLs[i]
		if url != expectedURL {
			t.Fatalf("actual url %q, expected url %q", url, expectedURL)
		}
	}
}

func createTestRRBalancer() LoadBalancer {
	locator := NewInMemLocator([]string{
		"test/test/test/test/1=1",
		"test/test/test/test/2=2",
		"test/test/test/test/3=3",
	})
	return NewRoundRobin(logger.NewNilLogger(), locator, discovery.LocationInfo{
		Namespace:   "test",
		Venture:     "test",
		Environment: "test",
		ServiceName: "test",
	})
}
