package balancer

import (
	"testing"
	"time"

	"bitbucket.org/lazadaweb/discovery/discovery"
	"bitbucket.org/lazadaweb/go-logger"
)

func TestStats_Create(t *testing.T) {
	stats := rttStats{}

	if stats.avg != 0 {
		t.FailNow()
	}
}

func TestStats_Add(t *testing.T) {
	stats := rttStats{}

	stats.Add(maxResponseTime)

	if stats.avg != maxResponseTime {
		t.Fatalf("expected %s, actual %s", maxResponseTime, stats.avg)
	}
}

func TestStats_AverageIs1Second_WhenFilledWith1Second(t *testing.T) {
	stats := rttStats{}

	for i := 0; i < rttHistorySize; i++ {
		stats.Add(time.Second)
	}

	if stats.avg != time.Second {
		t.Fatalf("expected %s, actual %s", time.Second, stats.avg)
	}
}

func TestStats_Add10Seconds(t *testing.T) {
	stats := rttStats{}

	for i := 0; i < rttHistorySize; i++ {
		stats.Add(time.Second)
	}
	stats.Add(rttHistorySize * time.Second)

	expected, _ := time.ParseDuration("1.240234375s")
	if stats.avg != expected {
		t.Fatalf("expected %s, actual %s", expected, stats.avg)
	}
}

func TestStats_AddOneSecond(t *testing.T) {
	stats := rttStats{}

	for i := 0; i < rttHistorySize; i++ {
		stats.Add(rttHistorySize * time.Second)
	}
	stats.Add(time.Second)

	expected, _ := time.ParseDuration("3.759765625s")
	if stats.avg != expected {
		t.Fatalf("expected %s, actual %s", expected, stats.avg)
	}
}

func TestWeightedRoundRobin_Next(t *testing.T) {
	balancer := createTestWRRBalancer()
	defer balancer.Stop()

	if url, _ := balancer.Next(); url != "1" {
		t.Fail()
	}
	if url, _ := balancer.Next(); url != "2" {
		t.Fail()
	}
	if url, _ := balancer.Next(); url != "3" {
		t.Fail()
	}
	if url, _ := balancer.Next(); url != "1" {
		t.Fail()
	}
	if url, _ := balancer.Next(); url != "2" {
		t.Fail()
	}
	if url, _ := balancer.Next(); url != "3" {
		t.Fail()
	}
}

func TestWeightedRoundRobin_NextN1(t *testing.T) {
	balancer := createTestWRRBalancer()
	defer balancer.Stop()

	assertNextN(t, balancer, 1, "1")
	assertNextN(t, balancer, 1, "2")
	assertNextN(t, balancer, 1, "3")
	assertNextN(t, balancer, 1, "1")
}

func TestWeightedRoundRobin_NextN2(t *testing.T) {
	balancer := createTestWRRBalancer()
	defer balancer.Stop()

	assertNextN(t, balancer, 2, "1", "2")
	assertNextN(t, balancer, 2, "2", "3")
	assertNextN(t, balancer, 2, "3", "1")
	assertNextN(t, balancer, 2, "1", "2")
}

func TestWeightedRoundRobin_NextN3(t *testing.T) {
	balancer := createTestWRRBalancer()
	defer balancer.Stop()

	assertNextN(t, balancer, 3, "1", "2", "3")
	assertNextN(t, balancer, 3, "2", "3", "1")
	assertNextN(t, balancer, 3, "3", "1", "2")
	assertNextN(t, balancer, 3, "1", "2", "3")
}

func TestWeightedRoundRobin_NextN4(t *testing.T) {
	balancer := createTestWRRBalancer()
	defer balancer.Stop()

	assertNextN(t, balancer, 4, "1", "2", "3")
	assertNextN(t, balancer, 4, "2", "3", "1")
	assertNextN(t, balancer, 4, "3", "1", "2")
	assertNextN(t, balancer, 4, "1", "2", "3")
}

func createTestWRRBalancer() LoadBalancer {
	locator := NewInMemLocator([]string{
		"test/test/test/test/1=1",
		"test/test/test/test/2=2",
		"test/test/test/test/3=3",
	})
	return NewWeightedRoundRobin(logger.NewNilLogger(), locator, discovery.LocationInfo{
		Namespace:   "test",
		Venture:     "test",
		Environment: "test",
		ServiceName: "test",
	})
}
