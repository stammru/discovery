package balancer

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"sync"

	"bitbucket.org/lazadaweb/discovery/discovery"
	etcd "github.com/coreos/etcd/client"
	"golang.org/x/net/context"
)

var _ Producer = (*StaticProducer)(nil)

// StaticProducer is DEPRECATED.
type StaticProducer struct {
	source   chan *etcd.Response
	stopOnce sync.Once
}

func NewStaticProducer(nodes Nodes) *StaticProducer {
	etcdNodes := make(etcd.Nodes, len(nodes))
	for i, node := range nodes {
		etcdNodes[i] = &etcd.Node{Key: node.Key, Value: node.URL}
	}
	resp := &etcd.Response{Action: "get", Node: &etcd.Node{Nodes: etcdNodes}}

	source := make(chan *etcd.Response, 1)
	source <- resp

	return &StaticProducer{source: source}
}

func (e *StaticProducer) Stream() chan *etcd.Response {
	return e.source
}

func (e *StaticProducer) Stop() {
	e.stopOnce.Do(func() {
		close(e.source)
	})
}

func StaticProducerFromFile(filename string, info discovery.LocationInfo) (*StaticProducer, error) {
	info.Property = discovery.NodesProperty

	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	key := info.StorageKey()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()

		vals := strings.Split(line, "=")
		if len(vals) != 2 {
			continue
		}

		if strings.HasPrefix(vals[0], key) {
			name := strings.TrimPrefix(vals[0], key)
			urls := strings.Split(vals[1], ",")
			nodes := make(Nodes, len(urls))
			for i, url := range urls {
				url := strings.TrimSpace(url)
				nodes[i] = &Node{Key: name, URL: url}
			}
			p := NewStaticProducer(nodes)
			return p, nil
		}
	}

	return nil, fmt.Errorf("nodes for %q not found in file %q", key, filename)
}

type inMemLocator struct {
	nodes []string
}

func NewInMemLocator(nodes []string) *inMemLocator {
	return &inMemLocator{nodes}
}

func (l *inMemLocator) Get(ctx context.Context, info discovery.LocationInfo) ([]discovery.ServiceInfo, error) {
	discoveryInfo := make([]discovery.ServiceInfo, 0, len(l.nodes))
	for _, v := range l.nodes {
		values := strings.Split(v, "=")
		discoveryInfo = append(discoveryInfo, discovery.ServiceInfo{Name: values[0], Value: values[1]})
	}
	return discoveryInfo, nil
}

func (l *inMemLocator) Locate(ctx context.Context, info discovery.LocationInfo, out chan *etcd.Response) error {
	etcdNodes := make(etcd.Nodes, 0, len(l.nodes))
	for _, v := range l.nodes {
		values := strings.Split(v, "=")
		etcdNodes = append(etcdNodes, &etcd.Node{Key: values[0], Value: values[1]})
	}
	out <- &etcd.Response{Action: "get", Node: &etcd.Node{Nodes: etcdNodes}}
	return nil
}
