package balancer

import etcd "github.com/coreos/etcd/client"

// Producer is DEPRECATED.
type Producer interface {
	Stream() chan *etcd.Response
	Stop()
}
