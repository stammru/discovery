package balancer

import (
	"net"
	"net/url"
	"time"
)

// DialCheck tries to connect to host:port over tcp
func DialCheck(rawURL string, timeout time.Duration) error {
	// TODO: parse it only once
	u, err := url.Parse(rawURL)
	if err != nil {
		return err
	}

	conn, err := net.DialTimeout("tcp", u.Host, timeout)
	if err != nil {
		return err
	}

	if err := conn.Close(); err != nil {
		return err
	}

	return nil
}
