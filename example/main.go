package main

import (
	"fmt"
	"log"
	"os"
	"sort"
	"time"

	"bitbucket.org/lazadaweb/discovery/balancer"
	"bitbucket.org/lazadaweb/discovery/discovery"
	"bitbucket.org/lazadaweb/discovery/discovery/etcd"
	gologger "bitbucket.org/lazadaweb/go-logger"
	etcdcl "github.com/coreos/etcd/client"
)

var logger = gologger.NewBuiltinLogger(log.New(os.Stdout, "", log.Ldate|log.Ltime), gologger.LevelDebug)

func main() {
	client := createEtcdClient()
	b := createBalancer(client)
	defer b.Stop()

	// print balancer statistics in a loop
	for {
		for i := 0; i < 1000; i++ {
			if _, err := b.Next(); err != nil {
				//panic(err)
			}
			time.Sleep(5 * time.Millisecond)
		}

		printStats(b)

		time.Sleep(100 * time.Millisecond)
	}
}

func createEtcdClient() etcdcl.Client {
	cfg := etcdcl.Config{
		Endpoints:               []string{"http://localhost:4001/"},
		HeaderTimeoutPerRequest: 5 * time.Second,
	}
	client, err := etcdcl.New(cfg)
	if err != nil {
		logger.Emergency("etcd cluster is unhealthy")
		os.Exit(1)
	}
	return client
}

func createBalancer(client etcdcl.Client) balancer.LoadBalancer {
	return balancer.NewWeightedRoundRobin(logger, etcd.NewLocator(client, logger),
		discovery.LocationInfo{
			Namespace:   "test_namespace",
			Venture:     "test_venture",
			Environment: "staging",
			ServiceName: "myservice",
		})
}

func printStats(b balancer.LoadBalancer) {
	stats := b.Stats()
	//	sort.Sort(StatsByValue(stats))
	sort.Sort(sort.Reverse(balancer.StatsByProbability(stats)))
	for _, v := range stats {
		state := " "
		if !v.Healthy {
			state = "-"
		}

		fmt.Printf("%s %s\t%4d  %5.2f%%  %-12s  %-12s\n", state, v.Key, v.HitCount, v.HitProbability*100., v.RTTAverage, v.RTT)
	}
	fmt.Print("\n================\n\n")
}
