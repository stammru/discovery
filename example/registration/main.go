// Example for registration a service in etcd.
// Here we register the service until we catch TERM/INT signal.
//
// ctx, cancel := context.WithCancel(context.Background())
// registrator.Register(ctx, ...)
// ...
// cancel() // after canceling the context, service registration will be deleted

package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"runtime"
	"sync"
	"syscall"
	"time"

	"bitbucket.org/lazadaweb/discovery/discovery"
	"bitbucket.org/lazadaweb/discovery/discovery/etcd"
	gologger "bitbucket.org/lazadaweb/go-logger"
	etcdcl "github.com/coreos/etcd/client"
	"golang.org/x/net/context"
)

var logger = gologger.NewBuiltinLogger(log.New(os.Stdout, "", log.Ldate|log.Ltime), gologger.LevelDebug)

func main() {
	var wg sync.WaitGroup

	ctx, cancel := context.WithCancel(context.Background())

	wg.Add(1)
	go func() {
		defer wg.Done()
		defer cancel()
		catchStopSignals()
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		registerServer(ctx)
	}()

	go func() {
		startTime := time.Now()
		lastNumGoroutine := runtime.NumGoroutine()
		for {
			numGoroutine := runtime.NumGoroutine()
			diff := numGoroutine - lastNumGoroutine
			lastNumGoroutine = numGoroutine
			fmt.Printf("Num of goroutines: %d(+%d), elapsed: %s\n", numGoroutine, diff, time.Since(startTime))
			time.Sleep(15 * time.Second)
		}
	}()

	wg.Wait()
	logger.Info("server stopped")
}

func catchStopSignals() {
	ch := make(chan os.Signal, 2)
	signal.Notify(ch, syscall.SIGINT, syscall.SIGTERM)
	<-ch
	signal.Stop(ch)
	logger.Info("received int/term signal")
}

func registerServer(ctx context.Context) {
	cfg := etcdcl.Config{
		Endpoints:               []string{"http://localhost:4001/"},
		HeaderTimeoutPerRequest: 5 * time.Second,
	}
	client, err := etcdcl.New(cfg)
	if err != nil {
		logger.Emergency("etcd cluster is unhealthy")
		os.Exit(1)
	}
	logger.Info("run registration")
	registrator := etcd.NewRegistrator(client, logger)
	registrator.Register(ctx, discovery.RegistrationInfo{
		Namespace:   "test_namespace",
		Venture:     "test_venture",
		Environment: "staging",
		ServiceName: "myservice",
		Property:    discovery.NodesProperty,
		Key:         "test_node",
		Value:       "http://localhost:8080/",
	})
	registrator.Register(ctx, discovery.RegistrationInfo{
		Namespace:   "test_namespace",
		Venture:     "test_venture",
		Environment: "staging",
		ServiceName: "myservice",
		Property:    discovery.VersionsProperty,
		Key:         "test_node",
		Value:       "1.0.0",
	})
}
