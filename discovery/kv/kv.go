package kv

import (
	"bufio"
	"os"
	"strconv"
	"strings"

	"bitbucket.org/lazadaweb/discovery/discovery"
	etcd "github.com/coreos/etcd/client"
)

type kvClient struct {
	services map[string][]string
}

type kvLocator struct {
	client *kvClient
	info   discovery.LocationInfo
	nodes  []string
	index  int
	logger discovery.ILogger
}

type kvRegistrator struct {
	client *kvClient
	info   discovery.RegistrationInfo
	logger discovery.ILogger
}

// NewClient create simple kv client for service location.
// It is useful during development or debugging and can be loaded from a file.
func NewClient(services map[string][]string) discovery.IClient {
	return &kvClient{services}
}

// LoadFromFile creates simple kv client for service location and populates it with values from a file.
// Format of the file looks like this:
// discovery_test/my/test/myservice1=url1,url2
// discovery_test/my/test/myservice2=url1
func LoadFromFile(name string) (discovery.IClient, error) {
	file, err := os.Open(name)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	kv := make(map[string][]string)
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		values := strings.SplitN(line, "=", 2)
		if len(values) != 2 {
			continue
		}
		key := values[0]
		value := values[1]
		kv[key] = strings.Split(value, ",")
	}
	return &kvClient{kv}, nil
}

func (c *kvClient) Close() {
}

func (c *kvClient) NewLocator(info discovery.LocationInfo, logger discovery.ILogger) discovery.IServiceLocator {
	if info.Property == "" {
		info.Property = discovery.NodesProperty
	}
	nodes := c.services[info.StorageKey()]
	l := &kvLocator{
		client: c,
		info:   info,
		nodes:  nodes,
		logger: logger,
	}

	// TODO: temporary hack
	if info.Output != nil {
		etcdNodes := make(etcd.Nodes, len(nodes))
		for i, node := range nodes {
			etcdNodes[i] = &etcd.Node{Key: strconv.Itoa(i), Value: node}
		}
		resp := &etcd.Response{Action: "get", Node: &etcd.Node{Nodes: etcdNodes}}
		go func() {
			info.Output <- resp
		}()
	}

	return l
}

func (c *kvClient) NewRegistrator(info discovery.RegistrationInfo, logger discovery.ILogger) discovery.IServiceRegistrator {
	return kvRegistrator{
		client: c,
		info:   info,
		logger: logger,
	}
}

func (l *kvLocator) Locate() (url string, err error) {
	if len(l.nodes) == 0 {
		return "", discovery.ErrNoServiceAvailable
	}
	if l.index >= len(l.nodes) {
		l.index = 0
	}
	node := l.nodes[l.index]
	l.index++
	return node, nil
}

func (l *kvLocator) Close() {
	if l.info.Output != nil {
		close(l.info.Output)
	}
}

func (r kvRegistrator) Run() {
	key := r.info.StorageKey()
	nodes := r.client.services[key]
	r.client.services[key] = append(nodes, r.info.Value)
}

func (r kvRegistrator) Close() {
}

// GetServices return list of services
func (l *kvLocator) GetServices() []discovery.ServiceInfo {
	return []discovery.ServiceInfo{}
}
