package kv

import (
	"testing"

	"bitbucket.org/lazadaweb/discovery/discovery"
	"bitbucket.org/lazadaweb/go-logger"
)

func TestKVClient(t *testing.T) {
	services := map[string][]string{
		"discovery_test/my/test/myservice/nodes/": []string{"url1", "url2"},
	}
	client := NewClient(services)
	locator := client.NewLocator(discovery.LocationInfo{
		Namespace:   "discovery_test",
		Venture:     "my",
		Environment: "test",
		ServiceName: "myservice",
		Property:    discovery.NodesProperty,
	}, logger.NewNilLogger())
	assertURLs(t, locator, []string{"url1", "url2", "url1"})

	registrator := client.NewRegistrator(discovery.RegistrationInfo{
		Namespace:   "discovery_test",
		Venture:     "my",
		Environment: "test",
		ServiceName: "test",
		Property:    discovery.NodesProperty,
		Key:         "test",
		Value:       "http://localhost",
	}, logger.NewNilLogger())
	registrator.Run()

	locator = client.NewLocator(discovery.LocationInfo{
		Namespace:   "discovery_test",
		Venture:     "my",
		Environment: "test",
		ServiceName: "test",
		Property:    discovery.NodesProperty,
	}, logger.NewNilLogger())
	url, err := locator.Locate()
	if err != nil {
		t.Fatalf("Could not locate service: %v", err)
	}
	if url != "http://localhost" {
		t.Fatal("Could not locate service")
	}
}

func TestLoadFromFile(t *testing.T) {
	client, err := LoadFromFile("test_conf.ini")
	if err != nil {
		t.Fatal(err)
	}

	locator1 := client.NewLocator(discovery.LocationInfo{
		Namespace:   "discovery_test",
		Venture:     "my",
		Environment: "test",
		ServiceName: "myservice1",
		Property:    discovery.NodesProperty,
	}, logger.NewNilLogger())
	assertURLs(t, locator1, []string{"url1", "url2"})

	locator2 := client.NewLocator(discovery.LocationInfo{
		Namespace:   "discovery_test",
		Venture:     "my",
		Environment: "test",
		ServiceName: "myservice2",
		Property:    discovery.NodesProperty,
	}, logger.NewNilLogger())
	assertURLs(t, locator2, []string{"url1"})
}

func assertURLs(t *testing.T, locator discovery.IServiceLocator, expectedURLs []string) {
	for i := 0; i < len(expectedURLs); i++ {
		url, err := locator.Locate()
		if err != nil {
			t.Fatalf("Could not locate service: %v", err)
		}
		if url != expectedURLs[i] {
			t.Fatal("Could not locate service")
		}
	}
}
