package etcd

import (
	//	"time"

	"bitbucket.org/lazadaweb/discovery/discovery"
	etcdcl "github.com/coreos/etcd/client"
	//	"golang.org/x/net/context"
)

type etcdClient struct {
	client etcdcl.Client
	//	ctx    context.Context
	//	cancel context.CancelFunc
}

// NewClient returns new etcd client
func NewClient(cfg etcdcl.Config, logger discovery.ILogger) (discovery.IClient, error) {
	client, err := etcdcl.New(cfg)
	if err != nil {
		return nil, err
	}

	//	ctx, cancel := context.WithCancel(context.Background())

	//	go func() {
	//		for {
	//			err := client.AutoSync(ctx, 500*time.Millisecond)
	//			if err == context.DeadlineExceeded || err == context.Canceled {
	//				break
	//			}
	//			logger.Warningf("etcd autosync error: %v", err)
	//			time.Sleep(500 * time.Millisecond)
	//		}
	//	}()

	return &etcdClient{
		client: client,
		//		ctx:    ctx,
		//		cancel: cancel,
	}, nil
}

func (c *etcdClient) Close() {
	//	c.cancel()
}
