package etcd

import (
	"time"

	"bitbucket.org/lazadaweb/discovery/discovery"
	etcdcl "github.com/coreos/etcd/client"
	"golang.org/x/net/context"
)

type etcdLocator struct {
	logger  discovery.ILogger
	keysAPI etcdcl.KeysAPI
}

func NewLocator(client etcdcl.Client, logger discovery.ILogger) discovery.IServiceLocator2 {
	return &etcdLocator{
		logger:  logger,
		keysAPI: etcdcl.NewKeysAPI(client),
	}
}

func (l *etcdLocator) Get(ctx context.Context, info discovery.LocationInfo) ([]discovery.ServiceInfo, error) {
	resp, err := l.keysAPI.Get(ctx, info.StorageKey(), &etcdcl.GetOptions{
		Recursive: false,
		Sort:      false,
	})
	if err != nil {
		return nil, err
	}

	services := make([]discovery.ServiceInfo, len(resp.Node.Nodes))
	for i, node := range resp.Node.Nodes {
		services[i] = discovery.ServiceInfo{Name: node.Key, Value: node.Value}
	}
	return services, nil
}

func (l *etcdLocator) Locate(parentCtx context.Context, info discovery.LocationInfo, out chan *etcdcl.Response) error {
	ctx, cancel := context.WithCancel(parentCtx)
	defer cancel()

	key := info.StorageKey()
	var backoff Backoff

	handlerError := func(err error) {
		l.logger.Errorf("unexpected etcd error: %v", err)

		if etcdErr, ok := err.(etcdcl.Error); ok {
			// Key not found
			if etcdErr.Code == etcdcl.ErrorCodeKeyNotFound {
				_, err := l.keysAPI.Set(ctx, key, "", &etcdcl.SetOptions{
					PrevExist: etcdcl.PrevNoExist,
					Dir:       true,
				})
				if err == nil {
					return
				}
				l.logger.Errorf("failed to create key %q: %v", key, err)
			}
		}

		// Prevent errors from consuming all resources.
		select {
		case <-time.After(backoff.Duration()):
		case <-ctx.Done():
		}
	}

	for {
		resp, err := l.keysAPI.Get(ctx, key, &etcdcl.GetOptions{Recursive: true})
		if err != nil {
			switch err {
			case context.DeadlineExceeded, context.Canceled:
				return err
			default:
				handlerError(err)
				continue
			}
		}
		waitIndex := resp.Index
		out <- resp
		backoff.Reset()

		watcher := l.keysAPI.Watcher(key, &etcdcl.WatcherOptions{
			AfterIndex: waitIndex,
			Recursive:  true,
		})
	WATCH:
		for waitIndex != 0 {
			resp, err := watcher.Next(ctx)
			if err != nil {
				switch err {
				case context.DeadlineExceeded, context.Canceled:
					return err
				default:
					handlerError(err)
					break WATCH
				}
			}
			waitIndex = resp.Node.ModifiedIndex
			out <- resp
			backoff.Reset()
		}
	}
}

// locator is DEPRECATED.
type locator struct {
	etcdLocator discovery.IServiceLocator2
	logger      discovery.ILogger
	info        discovery.LocationInfo
	ctx         context.Context
	cancel      context.CancelFunc
}

// NewLocator creates ServiceLocator which is used to locate services in etcd. DEPRECATED.
func (c *etcdClient) NewLocator(info discovery.LocationInfo, logger discovery.ILogger) discovery.IServiceLocator {
	if info.Output == nil {
		panic("output receiver is nil")
	}

	l := &locator{
		etcdLocator: NewLocator(c.client, logger),
		logger:      logger,
		info:        info,
	}
	l.ctx, l.cancel = context.WithCancel(context.Background())

	go func() {
		defer close(info.Output)
		l.etcdLocator.Locate(l.ctx, info, info.Output)
	}()

	return l
}

// Close stops Locator.
func (l *locator) Close() {
	l.cancel()
}

func (l *locator) Locate() (string, error) {
	return "", discovery.ErrNoServiceAvailable
}

// GetServices return list of services
func (l *locator) GetServices() []discovery.ServiceInfo {
	services, err := l.etcdLocator.Get(l.ctx, l.info)
	if err != nil {
		l.logger.Errorf("could not get services: %v", err)
	}
	return services
}
