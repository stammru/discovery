package etcd

import (
	"strings"
	"time"

	"bitbucket.org/lazadaweb/discovery/discovery"
	etcdcl "github.com/coreos/etcd/client"
	"golang.org/x/net/context"
)

const defaultKeyTTL = 2 * time.Minute

type etcdRegistrator struct {
	logger  discovery.ILogger
	keysAPI etcdcl.KeysAPI
}

// NewRegistrator creates ServiceRegistrator which will update service information in etcd in certain intervals.
func NewRegistrator(client etcdcl.Client, logger discovery.ILogger) discovery.IServiceRegistrator2 {
	return &etcdRegistrator{
		logger:  logger,
		keysAPI: etcdcl.NewKeysAPI(client),
	}
}

// Run starts service registration in certain intervals. This method is blocking.
func (r *etcdRegistrator) Register(ctx context.Context, info discovery.RegistrationInfo) {
	if info.TTL == 0 {
		info.TTL = defaultKeyTTL
	}
	if info.Interval == 0 {
		// choose registration interval smaller than expiration time of registration info to keep it alive
		info.Interval = info.TTL / 2
	}
	key := info.StorageKey() + info.Key

	value := info.Value
	// temporary hack, GO-4096
	if info.Property == discovery.NodesProperty {
		if i := strings.LastIndex(value, ".iddc"); i != -1 {
			value = value[:i] + "-pub" + value[i:]
		}
	}

	// We must pass background context to unregister() because defer unregisration will work when original context
	// is already canceled.
	defer r.unregister(context.Background(), key)

	ticker := time.NewTicker(info.Interval)
	defer ticker.Stop()

	for {
		registerCtx, cancelRegister := context.WithTimeout(ctx, info.Interval)
		go r.register(registerCtx, key, value, info.TTL)

		select {
		case <-ctx.Done():
			cancelRegister()
			return
		case <-ticker.C:
			cancelRegister()
		}
	}
}

func (r *etcdRegistrator) register(ctx context.Context, key, value string, ttl time.Duration) {
	r.logger.Debugf("register %q=%q, ttl: %s", key, value, ttl)

	_, err := r.keysAPI.Set(ctx, key, value, &etcdcl.SetOptions{TTL: ttl, PrevExist: etcdcl.PrevIgnore})
	if err != nil && err != context.DeadlineExceeded && err != context.Canceled {
		r.logger.Warningf("could not register %q: %v", key, err)
	}
}

func (r *etcdRegistrator) unregister(ctx context.Context, key string) {
	r.logger.Debugf("unregister %q", key)

	_, err := r.keysAPI.Delete(ctx, key, &etcdcl.DeleteOptions{})
	if err != nil && err != context.DeadlineExceeded && err != context.Canceled {
		r.logger.Warningf("could not unregister %q: %v", key, err)
	}
}

// Old version of etcd Registrator
type registrator struct {
	etcdRegistrator discovery.IServiceRegistrator2
	info            discovery.RegistrationInfo
	ctx             context.Context
	cancel          context.CancelFunc
}

// NewRegistrator creates ServiceRegistrator which will update service information in etcd in certain intervals.
func (c *etcdClient) NewRegistrator(info discovery.RegistrationInfo, logger discovery.ILogger) discovery.IServiceRegistrator {
	ctx, cancel := context.WithCancel(context.Background())
	return &registrator{
		etcdRegistrator: NewRegistrator(c.client, logger),
		info:            info,
		ctx:             ctx,
		cancel:          cancel,
	}
}

// Run starts service registration in certain intervals in goroutine.
func (r *registrator) Run() {
	go r.etcdRegistrator.Register(r.ctx, r.info)
}

// Close stops Registrator.
func (r *registrator) Close() {
	r.cancel()
}
