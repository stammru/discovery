package discovery

import (
	"errors"
	"fmt"
	"time"

	etcdcl "github.com/coreos/etcd/client"
	"golang.org/x/net/context"
)

// IClient lets create IServiceRegistrator and IServiceLocator.
type IClient interface {
	NewRegistrator(info RegistrationInfo, logger ILogger) IServiceRegistrator
	NewLocator(info LocationInfo, logger ILogger) IServiceLocator
	Close()
}

// LocationInfo contains service discovery information.
type LocationInfo struct {
	Namespace   string
	Venture     string
	Environment string
	ServiceName string
	Property    string
	// Output is DEPRECATED
	Output chan *etcdcl.Response
}

type ServiceInfo struct {
	Name  string
	Value string
}

// IServiceLocator helps to locate services. DEPRECATED.
type IServiceLocator interface {
	Locate() (value string, err error)
	GetServices() []ServiceInfo
	Close()
}

// IServiceLocator2 helps to locate services.
type IServiceLocator2 interface {
	Get(ctx context.Context, info LocationInfo) ([]ServiceInfo, error)
	Locate(ctx context.Context, info LocationInfo, out chan *etcdcl.Response) error
}

// RegistrationInfo contains service discovery information.
type RegistrationInfo struct {
	Namespace   string
	Venture     string
	Environment string
	ServiceName string
	Property    string
	// Service info TTL
	TTL time.Duration
	// Service info update interval
	Interval time.Duration
	Key      string
	Value    string
}

// IServiceRegistrator registers service.
type IServiceRegistrator interface {
	Run()
	Close()
}

// IServiceRegistrator2 registers service.
type IServiceRegistrator2 interface {
	Register(ctx context.Context, info RegistrationInfo)
}

const (
	// NodesProperty used to discovery nodes
	NodesProperty = "nodes"
	// VersionsProperty used to discovery nodes versions
	VersionsProperty = "versions"
)

// ErrNoServiceAvailable indicates that there are no services to be returned in Locate() method.
var ErrNoServiceAvailable = errors.New("No service available")

// Key returns full key in key-value store
func (i LocationInfo) StorageKey() string {
	return fmt.Sprintf("%s/%s/%s/%s/%s/", i.Namespace, i.Venture, i.Environment, i.ServiceName, i.Property)
}

// Key returns full key in key-value store
func (i RegistrationInfo) StorageKey() string {
	return fmt.Sprintf("%s/%s/%s/%s/%s/", i.Namespace, i.Venture, i.Environment, i.ServiceName, i.Property)
}
